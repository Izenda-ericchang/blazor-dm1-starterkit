USE [master]
GO
/****** Object:  Database [blazor_userdb]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE DATABASE [blazor_userdb]
GO
ALTER DATABASE [blazor_userdb] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [blazor_userdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [blazor_userdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [blazor_userdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [blazor_userdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [blazor_userdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [blazor_userdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [blazor_userdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [blazor_userdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [blazor_userdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [blazor_userdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [blazor_userdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [blazor_userdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [blazor_userdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [blazor_userdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [blazor_userdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [blazor_userdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [blazor_userdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [blazor_userdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [blazor_userdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [blazor_userdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [blazor_userdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [blazor_userdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [blazor_userdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [blazor_userdb] SET RECOVERY FULL 
GO
ALTER DATABASE [blazor_userdb] SET  MULTI_USER 
GO
ALTER DATABASE [blazor_userdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [blazor_userdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [blazor_userdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [blazor_userdb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [blazor_userdb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'blazor_userdb', N'ON'
GO
ALTER DATABASE [blazor_userdb] SET QUERY_STORE = OFF
GO
USE [blazor_userdb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[Discriminator] [nvarchar](max) NOT NULL,
	[TenantId] [int] NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tenants]    Script Date: 1/25/2021 4:28:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tenants](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Tenants] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'00000000000000_CreateIdentitySchema', N'3.1.7')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201207190458_IdentitySupport', N'3.1.7')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210122144641_applicationUserUpdate', N'3.1.7')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210122144922_tenantIdUpdate', N'3.1.7')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'6b9069cf-ea6d-44e5-98a9-4641fc6bef47', N'Manager', N'MANAGER', N'26de8ef1-1bae-4b5e-942f-336b7682ea65')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'966b78c6-d215-43e1-b5ba-2ca06dc80ed0', N'Admin', N'ADMIN', N'4369e7cd-2b28-409e-b18e-52a7153e1985')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'cdda965c-f28f-471a-93b5-ab1bdf1f159e', N'VP', N'VP', N'8f4a739c-bc9c-440f-a4f8-e69ea466ca69')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'd72d3167-28ab-4ba8-8d33-db64f6de5c89', N'Employee', N'EMPLOYEE', N'be5e9e0f-7e69-4d02-a4e7-bb1a4437582b')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'11ae3c97-4f6f-4152-b7ab-35211e040141', N'6b9069cf-ea6d-44e5-98a9-4641fc6bef47')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9345d683-634e-4ec1-a676-250e1fee9e67', N'6b9069cf-ea6d-44e5-98a9-4641fc6bef47')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'97a7ccc2-ba48-4960-b85e-649a922ee382', N'6b9069cf-ea6d-44e5-98a9-4641fc6bef47')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1b584c7f-358e-4391-a52c-60ecf519e8f1', N'966b78c6-d215-43e1-b5ba-2ca06dc80ed0')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'49cb9f52-9519-427e-8e7b-6211425bb237', N'cdda965c-f28f-471a-93b5-ab1bdf1f159e')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'515721c6-d882-4b8d-9de7-f13e48117211', N'cdda965c-f28f-471a-93b5-ab1bdf1f159e')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a88a1a7e-5d1b-4d84-8969-4078a646943d', N'cdda965c-f28f-471a-93b5-ab1bdf1f159e')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'59c52543-0df6-4ec1-ae18-b6566cab1424', N'd72d3167-28ab-4ba8-8d33-db64f6de5c89')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'bfbc1b69-b963-41a8-9efa-ffcf9c24ed8c', N'd72d3167-28ab-4ba8-8d33-db64f6de5c89')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ce3bc894-6750-40d3-a7e0-bd9cef10982b', N'd72d3167-28ab-4ba8-8d33-db64f6de5c89')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'11ae3c97-4f6f-4152-b7ab-35211e040141', N'manager@retcl.com', N'MANAGER@RETCL.COM', N'manager@retcl.com', N'MANAGER@RETCL.COM', 0, N'AQAAAAEAACcQAAAAEAhU090/a24p8fSInZOOry3+CqKs9BO0mS6SrSEUHxq/CvwNviQrvqLbCwcwXVhVNw==', N'N2HRLK4ACRGDDUKDQRHTPOTZFZPIQ5IT', N'd3b48d4d-a7d2-42f6-a7aa-cb0a12c5e448', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 3)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'1b584c7f-358e-4391-a52c-60ecf519e8f1', N'IzendaAdmin@system.com', N'IZENDAADMIN@SYSTEM.COM', N'IzendaAdmin@system.com', N'IZENDAADMIN@SYSTEM.COM', 0, N'AQAAAAEAACcQAAAAEHSdt7kGkSdT3vySH5hS5ERmyaWOdwevxY/m18qkiqQAlUKuSSnIkvs3AVNwAAkNrA==', N'HNVBMFD5MFN3XX6A2YPRDF6XKGV5YWBT', N'4ddbe04a-897a-4925-a217-11c49ae5ecba', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'49cb9f52-9519-427e-8e7b-6211425bb237', N'vp@natwr.com', N'VP@NATWR.COM', N'vp@natwr.com', N'VP@NATWR.COM', 0, N'AQAAAAEAACcQAAAAEMW6nL1a0qOiepouJQgQRRsA0On4DmRcgDCC6yLDKLqAWnr2Dw18/nuVKZHFC7eT0Q==', N'Z2VKOVE5ES762BN7N27QMZN4GGN5LAPK', N'81e236fb-d93e-4bae-b7ce-8d4cea1c5273', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'515721c6-d882-4b8d-9de7-f13e48117211', N'vp@retcl.com', N'VP@RETCL.COM', N'vp@retcl.com', N'VP@RETCL.COM', 0, N'AQAAAAEAACcQAAAAEC9Tlt/QGej/aPvvUjv1hrMw1IyjFtffrq0iV0cJa75p1zFVJmrBNu5q2CxzUO8KUQ==', N'LDQGQLJHHZPFPCAAIISV4LJZSQWD7CZH', N'b66ba262-0b18-4592-b9c4-0bfa28767e32', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 3)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'59c52543-0df6-4ec1-ae18-b6566cab1424', N'employee@retcl.com', N'EMPLOYEE@RETCL.COM', N'employee@retcl.com', N'EMPLOYEE@RETCL.COM', 0, N'AQAAAAEAACcQAAAAEBW1sW9+B0u9dXlo29vpRzUn6XaXJ28oe6+VDj2l7b+TC7sY/G3Bz17Q3JxKR64j3Q==', N'CS42NJWGEVNX5HTDXMW7VX6B2KJLI7SL', N'2ffb64e6-352a-4856-a5cc-1237b10bae30', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 3)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'9345d683-634e-4ec1-a676-250e1fee9e67', N'manager@natwr.com', N'MANAGER@NATWR.COM', N'manager@natwr.com', N'MANAGER@NATWR.COM', 0, N'AQAAAAEAACcQAAAAEMCCcF7JI3hUCVQOgbPuyn5/pVRKfynrNBen/35pCDZXDM6bwOKFRTOB3EajXpA67g==', N'K34JHGTJ4BEGDYSSF6WMTNOZV37JKCZH', N'f2b67a36-e4e4-4f7b-adfe-2ff7987b73d0', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'97a7ccc2-ba48-4960-b85e-649a922ee382', N'manager@deldg.com', N'MANAGER@DELDG.COM', N'manager@deldg.com', N'MANAGER@DELDG.COM', 0, N'AQAAAAEAACcQAAAAEMSKbB/ux0S8VXyrFGrk64yYh+rimw3FD2hpKJ83Af5/zKbxA8CBWSYSDg+KZZC32g==', N'G5K3GAXR2LJ6ZNY3N665GGQ4HBQIW3TZ', N'12b931aa-4622-424d-b7dc-35e3bd239dac', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 1)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'a88a1a7e-5d1b-4d84-8969-4078a646943d', N'vp@deldg.com', N'VP@DELDG.COM', N'vp@deldg.com', N'VP@DELDG.COM', 0, N'AQAAAAEAACcQAAAAEJiFICJW4caillCNPVoP+75o7/4xl5FfRnOldGa3QQeDkCxA5xWMRVN9dUaK4mHNTA==', N'RT3XKFG5GVGGXXDU5UGZBE6E3JGWAD4C', N'2bb4d2cd-6514-4023-bf5b-e4906e8568f4', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 1)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'bfbc1b69-b963-41a8-9efa-ffcf9c24ed8c', N'employee@natwr.com', N'EMPLOYEE@NATWR.COM', N'employee@natwr.com', N'EMPLOYEE@NATWR.COM', 0, N'AQAAAAEAACcQAAAAENUSx/xI9Haxd1WdspZhhpKTIs6lvYyBN6FKsEM8GUnnLi5Ls88C8bU8m5UI+LZDTA==', N'RINKDYASQWG3LRZUWHMKF3GSJ5CCBJKP', N'66ae84c0-bd43-4a79-87c7-b17bff25c2bb', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 2)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Discriminator], [TenantId]) VALUES (N'ce3bc894-6750-40d3-a7e0-bd9cef10982b', N'employee@deldg.com', N'EMPLOYEE@DELDG.COM', N'employee@deldg.com', N'EMPLOYEE@DELDG.COM', 0, N'AQAAAAEAACcQAAAAEKhSwWb3AaBzo+PCYPU8KIh5dQW3MYIu63MkknWhNXHdVCtLJAbb0/yM6Ciw+WKCcA==', N'SPLIPCLO65NX54XD4OQWM4GK4Y3YCA64', N'e8613a24-622b-4c80-9680-ee0430eb5836', NULL, 0, 0, NULL, 1, 0, N'ApplicationUser', 1)
SET IDENTITY_INSERT [dbo].[Tenants] ON 

INSERT [dbo].[Tenants] ([Id], [Name]) VALUES (1, N'DELDG')
INSERT [dbo].[Tenants] ([Id], [Name]) VALUES (2, N'NATWR')
INSERT [dbo].[Tenants] ([Id], [Name]) VALUES (3, N'RETCL')
SET IDENTITY_INSERT [dbo].[Tenants] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_AspNetUsers_TenantId]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUsers_TenantId] ON [dbo].[AspNetUsers]
(
	[TenantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 1/25/2021 4:28:36 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT (N'') FOR [Discriminator]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_Tenants_TenantId] FOREIGN KEY([TenantId])
REFERENCES [dbo].[Tenants] ([Id])
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_AspNetUsers_Tenants_TenantId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
USE [master]
GO
ALTER DATABASE [blazor_userdb] SET  READ_WRITE 
GO
