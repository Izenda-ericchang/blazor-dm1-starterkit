﻿namespace BlazorDM1.Utility
{
    // Static Detail class
    public static class SD
    {
        #region Predefined Roles
        public const string Role_Employee = "Employee";
        public const string Role_Manager = "Manager";
        public const string Role_VP = "VP";
        public const string Role_Admin = "Admin";
        #endregion
    }
}
