﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web;

namespace BlazorDM1.Controllers
{
    public class ReportController : Controller
    {
        #region Methods
        public IActionResult Index() => NotFound();

        /// <summary>
        /// Render Report Viewer for sub report
        /// </summary>
        /// <param name="id">Report id used for rendering</param>
        /// <returns>ReportViewer View</returns>
        // [Route("ReportViewer/{id}")] // this is for routing from nav bar menu. Currently handled by Blazor component (ReportViewer.razor).
        // We need to remove this MVC pattern (parameter problems)
        [Route("izenda/report/view/{id}")]
        public IActionResult SubReportViewer(string id)
        {
            // TODO: need to figure out how to handle this filters object and pass it to _Host.cshtml from ReportViewer.razor page.
            var query = Request.Query;
            dynamic filters = new ExpandoObject();
            foreach (string key in query.Keys)
            {
                ((IDictionary<string, object>)filters).Add(key, query[key]);
            }

            ViewBag.overridingFilterQueries = JsonConvert.SerializeObject(filters);
            ViewBag.Id = id;
            return View();
        }

        /// <summary>
        /// Action for rendering Izenda Report Part
        /// Use case example: export pdf
        /// </summary>
        /// <param name="id">report part id</param>
        /// <param name="token">token from url</param>
        /// <returns>ReportPart View</returns>
        [Route("izenda/viewer/reportpart/{id}")]
        public IActionResult ReportPart(Guid id, string token)
        {
            ViewBag.Id = id;
            ViewBag.Token = string.IsNullOrWhiteSpace(token) ? HttpUtility.UrlEncode(Request.Cookies["access_token"]) : token;

            return View();
        }

        /// <summary>
        /// Action for rendering Izenda I-Frame from exported URL
        /// </summary>
        /// <param name="id">report id</param>
        /// <returns>IframeViewer View</returns>
        [Route("izenda/report/iframe/{id}")]
        public IActionResult IframeViewer(string id)
        {
            // TODO: need to figure out how to handle this filters object and pass it to _Host.cshtml from ReportViewer.razor page.
            var query = Request.Query;
            dynamic filters = new ExpandoObject();
            foreach (string key in query.Keys)
            {
                ((IDictionary<string, object>)filters).Add(key, query[key]);
            }

            ViewBag.overridingFilterQueries = JsonConvert.SerializeObject(filters);
            ViewBag.Id = id;
            return View();
        }
        #endregion
    }
}
