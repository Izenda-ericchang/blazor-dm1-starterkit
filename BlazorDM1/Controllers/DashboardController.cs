﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Dynamic;

namespace BlazorDM1.Controllers
{
    public class DashboardController : Controller
    {
        #region Methods
        public IActionResult Index()
        {
            // not supposed to be routed Index view
            return NotFound();
        }

        /// <summary>
        /// Action for rendering a dashboard viewer.
        /// </summary>
        /// <param name="id">The dashboard ID</param>
        /// <returns>DashboardViewer View</returns>
        [Route("izenda/izenda/dashboard/edit/{id}")]
        public IActionResult DashboardViewer(string id)
        {
            var query = Request.Query;
            dynamic filters = new ExpandoObject();
            foreach (string key in query.Keys)
            {
                ((IDictionary<string, object>)filters).Add(key, query[key]);
            }

            ViewBag.Id = id;
            ViewBag.filters = JsonConvert.SerializeObject(filters);
            return View();
        } 
        #endregion
    }
}
