﻿using BlazorDM1.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlazorDM1.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        #region Properties
        public DbSet<Tenant> Tenants { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        #endregion

        #region CTOR
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
        {
        } 
        #endregion
    }
}
