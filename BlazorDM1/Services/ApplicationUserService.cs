﻿using BlazorDM1.Data;
using BlazorDM1.IzendaBoundary;
using BlazorDM1.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlazorDM1.Services
{
    public class ApplicationUserService
    {
        #region Variables
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        #endregion

        #region CTOR
        public ApplicationUserService(ApplicationDbContext db, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) 
        {
            _db = db;
            _userManager = userManager;
            _roleManager = roleManager;
        }
        #endregion

        #region Methods
        public async Task<IEnumerable<ApplicationUser>> GetAllUsers() => await _db.ApplicationUsers.Include("Tenant").ToListAsync();

        public IEnumerable<Tenant> GetAllTenants() => _db.Tenants?.ToList();

        public async Task<List<string>> GetAllRoles(string tenantName = null)
        {
            var adminToken = IzendaTokenAuthorization.GetIzendaAdminToken();
            var izendaTenant = await IzendaUtilities.GetIzendaTenantByName(tenantName, adminToken);
            var roleDetailsByTenant = await IzendaUtilities.GetAllIzendaRoleByTenant(izendaTenant?.Id ?? null, adminToken);

            var roleList = new List<string>();
            foreach (var roleDetail in roleDetailsByTenant)
            {
                roleList.Add(roleDetail.Name);
            }

            return roleList;
        }

        public async Task<ApplicationUser> FindTenantUserAsync(string tenant, string username)
        {
            ApplicationUser user = null;

            if (!string.IsNullOrWhiteSpace(tenant))
            {
                user = await _db.ApplicationUsers
                    .Include(u => u.Tenant)
                    .Where(u => u.UserName.Equals(username))
                    .Where(u => u.Tenant.Name.Equals(tenant))
                    .SingleOrDefaultAsync();
            }
            else
            {
                user = await _db.ApplicationUsers
                    .Include(u => u.Tenant)
                    .Where(u => u.UserName.Equals(username))
                    .Where(u => !u.TenantId.HasValue)
                    .SingleOrDefaultAsync();
            }

            return user;
        }

        public async Task<ApplicationUser> GetFirstOrDefaultAsync(Expression<Func<ApplicationUser, bool>> filter = null, string includeProperties = null)
        {
            IQueryable<ApplicationUser> query = _db.Set<ApplicationUser>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }

            return await query.FirstOrDefaultAsync();
        }

        public int? GetTenantId(string name)
        {
            if (_db.Tenants.Any())
            {
                var tenant = _db.Tenants.Where(x => x.Name.Equals(name)).SingleOrDefault();
                return tenant?.Id;
            }
            else
                return null;
        }

        public Tenant GetTenantByName(string tenantName)
        {
            if (_db.Tenants.Any())
            {
                var tenant = _db.Tenants.Where(x => x.Name.Equals(tenantName)).SingleOrDefault();
                return tenant;
            }
            else
                return null;
        }

        public Task<IdentityResult> AddApplicationUser(ApplicationUser user) => _userManager.CreateAsync(user);

        public Task<IdentityRole> FindRoleByRoleName(string roleName) => _roleManager.FindByNameAsync(roleName);

        public Task<IdentityResult> CreateRole(string assignedRole) => _roleManager.CreateAsync(new IdentityRole(assignedRole));

        public Task<IdentityResult> AddRoleToUser(ApplicationUser newUser, string assignedRole) => _userManager.AddToRoleAsync(newUser, assignedRole);
        #endregion
    }
}
