﻿using BlazorDM1.Data;
using BlazorDM1.Models;
using System.Collections.Generic;
using System.Linq;

namespace BlazorDM1.Services
{
    public class TenantService
    {
        #region Variables
        private readonly ApplicationDbContext _db;
        #endregion

        #region CTOR
        public TenantService(ApplicationDbContext db) => _db = db;
        #endregion

        #region Methods
        public Tenant GetTenantByName(string name)
        {
            if (_db.Tenants.Any())
            {
                var tenant = _db.Tenants.Where(x => x.Name.Equals(name)).SingleOrDefault();
                return tenant;
            }
            else
                return null;
        }

        public void AddTenant(Tenant newTenant) => _db.Tenants.Add(newTenant);

        public IEnumerable<Tenant> GetAllTenants() => _db.Tenants?.ToList();

        public void SaveChanges() => _db.SaveChangesAsync();
        #endregion
    }
}
