﻿using System.ComponentModel.DataAnnotations;

namespace BlazorDM1.Services
{
    public class NewUserObject
    {
        #region Properties
        [Display(Name = "Tenant*")]
        public string SelectedTenant { get; set; }

        [Display(Name = "Role")]
        public string SelectedRole { get; set; }

        [Display(Name = "Is Admin")]
        public bool IsAdmin { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        #endregion
    }
}
