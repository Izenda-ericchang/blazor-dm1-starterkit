﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorDM1.Models
{
    public class ApplicationUser : IdentityUser
    {
        #region Properties
        public int? TenantId { get; set; }

        [ForeignKey("TenantId")]
        public Tenant Tenant { get; set; }

        [NotMapped]
        public string Role { get; set; }
        #endregion
    }
}
