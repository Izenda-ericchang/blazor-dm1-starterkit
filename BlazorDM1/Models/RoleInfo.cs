﻿using System;

namespace BlazorDM1.Models
{
    public class RoleInfo
    {
        #region Properties
        public Guid Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
