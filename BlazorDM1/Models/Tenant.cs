﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlazorDM1.Models
{
    public class Tenant
    {
        #region Properties
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Tenant ID")]
        public string Name { get; set; }

        [NotMapped]
        [Required]
        [Display(Name = "Tenant Detail Name")]
        public string TenantDetailName { get; set; }
        #endregion
    }
}
